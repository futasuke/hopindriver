import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import NetInfo from "@react-native-community/netinfo";
import axios from 'axios';
import Pusher from 'pusher-js/react-native';
import Echo from 'laravel-echo';
import io from "socket.io-client";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
let pusher = "";
let test = "";

export default class LoginScreen extends React.Component {
    state = {
        email: '',
        password: '',
        ip: 'http://192.168.1.107:8080',
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: '',
        userDetail: [],
    }

    checkInput() {
        if (this.state.email != '') {
            if (this.state.password != '') {
                this.tryLogin();
            }
            else {
                this.setState({ alertMsg: 'Please enter password' });
                this.setState({ modalVisible: true });
            }
        }
        else {
            this.setState({ alertMsg: 'Please enter email' });
            this.setState({ modalVisible: true });
        }

    }

    goToRegister() {
        var self = this;
        this.props.navigation.navigate('Register', { ip: this.state.ip });
    }

    tryLogin() {
        var self = this;
        this.setState({ showSpinner: true });

        axios.post(self.state.ip + '/api/driver-login', {
            email: self.state.email,
            password: self.state.password,
        }).then(function (response) {
            if (response.data[0].message == 'UploadDocument') {
                self.setState({ userDetail: response.data[0].content, userId: response.data[0].content.id });
                self.setState({ showSpinner: false });
                self.props.navigation.navigate('DriverRegister', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                });
            }
            else if (response.data[0].message == 'FirstDetail') {
                self.setState({ userDetail: response.data[0].content, userId: response.data[0].content.id });
                self.setState({ showSpinner: false });
                self.props.navigation.navigate('FirstDetail', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                    dpImage: self.state.ip + "/" + self.state.userDetail.dpPath,
                })
            }
            else if (response.data[0].message == 'Success') {
                self.setState({ userDetail: response.data[0].content, userId: response.data[0].content.id });
                self.setState({ showSpinner: false });
                self.props.navigation.navigate('HomeDrawer', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                    dpImage: self.state.ip + "/" + self.state.userDetail.dpPath,
                });
            } else {
                self.setState({ showSpinner: false });
                self.setState({ alertMsg: response.data[0].message });
                self.setState({ modalVisible: true })
            }
        }).catch(function (error) {
            self.setState({ showSpinner: false });
            alert(error);
            console.log(error);
        });
    }

    componentDidMount() {
        // pusher = new Pusher('ABCDE', {
        //     wsHost:'192.168.0.125',
        //     httpHost:'192.168.0.125',
        //     wsPort:'6001',
        //     httpPort:'8080',
        //     cluster:'mt1',
        // });

        // test = pusher.subscribe('home');
        // test.bind('pusher:subscription_succeeded', function () {
        //     console.log('asdasd')
        //     test.bind('App\\Events\\NewMessage',(e)=>{
        //         console.log(e.message);
        //     })   
        //     test.trigger('client-App\\Events\\NewMessage','test');
        // });   

        // var socket = new WebSocket('ws://192.168.0.125:6001/app/ABCDE');      
        // socket.onopen = function (params) {
        //     socket.send(JSON.stringify());
        //     socket.send('test');
        //     console.log('tring');
        // }
        // socket.onerror = function (error) {
        //     console.log('test');
        // }
        // socket.onmessage =  function (params) {
        //     console.log(params.data);
        // }
        

        // var echo = new Echo({
        //     host: 'ws://192.168.0.125:6001/app/ABCDE',
        //     broadcaster: 'socket.io',
        //     client: io,
        // });
        
        // console.log(echo);

        // var test = echo.channel('home');

        // console.log(test);
    }

    componentWillUnmount() {

    }

    triggerEvent() {
        console.log('woit');
        test.trigger('client-App\\Events\\NewMessage', 'anak setan');
    }

    render() {
        return (
            <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.logoContainer}><Image source={require('../assets/logoSmall.png')} style={styles.logo}></Image></View>

                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>

                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                        <Item rounded style={styles.emailContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/email.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Email"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType="email-address"
                                onChangeText={(email) => this.setState({ email })} />
                        </Item>

                        <Item rounded style={styles.passwordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ password })} />
                        </Item>

                        <TouchableOpacity style={styles.loginButton} onPress={() => this.checkInput()}>
                            <Text style={styles.buttonLabel}>Login</Text>
                        </TouchableOpacity>

                        <View style={styles.registerContainer}>
                            <Text style={styles.registerText1}>Don't have an account?</Text><TouchableOpacity onPress={() => this.goToRegister()}><Text style={styles.registerText2}>Sign Up Now</Text></TouchableOpacity>
                        </View>

                        <View>
                            <TouchableOpacity onPress={()=>alert(this.state.ip)}>
                                <Text style={styles.forgotText}>Forgot password?</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
    },
    container: {
        // justifyContent:"center",
        marginTop: 150,
        alignItems: "center",
        flex: 1,
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 20,
    },
    logoContainer: {
        borderColor: 'white',
        borderStyle: 'solid',
        borderWidth: 2,
        marginBottom: 100,
        borderRadius: 20,
    },
    emailContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    passwordContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 60,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 33,
        height: 25,
    },
    loginButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 60,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    registerContainer: {
        flexDirection: "row",
        marginBottom: 70,
    },
    registerText1: {
        color: 'white',
        fontSize: 18,
        marginRight: 15,
    },
    registerText2: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
        fontSize: 18,
    },
    forgotText: {
        color: "white",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
        // alignItems: "center",
        // justifyContent: "center",
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    }
})