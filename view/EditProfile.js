import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import ImagePicker, { clean } from 'react-native-image-crop-picker';
import axios from 'axios';
import FastImage from 'react-native-fast-image';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class EditProfile extends React.Component {
    state = {
        modalTitle: '',
        userId: this.props.route.params.userId,
        userDetail: '',
        file: '',
        ip: this.props.route.params.ip,
        dpImage: '',
        modalVisible: false,
        modalInput: '',
        datetime: '',
        key: '',
        alertVisible: false,
        alertMsg: '',
    }

    updateUser() {
        var self = this;
        axios.get(this.state.ip + '/api/driver-getDriver/' + this.state.userId)
            .then(function (response) {
                self.setState({ userDetail: response.data[0].content });
                self.setState({ dpImage: self.state.ip + "/" + response.data[0].content.dpPath });
            }).catch(function (error) {
                console.log(error);
            });
    }

    goBackScreen() {
        this.props.navigation.navigate('Setting',{
            userDetail: this.state.userDetail,
            dpImage: this.state.dpImage,
        });
    }

    openModal(title) {
        this.setState({ modalTitle: title });
        this.setState({ modalVisible: true });
    }

    selectImage() {
        var self = this;
        ImagePicker.openPicker({
            width: 250,
            height: 250,
            cropping: true,
            mediaType: 'photo',
            includeBase64: true,
        }).then(function (image) {    
            self.setState({ file: image });
            self.setState({ dpImage: "" });
            self.uploadImage();
        }).catch(function (error) {
            console.log(error);
            if(error == "Error: Cannot find image data"){
                self.setState({alertMsg:"Only JPG or PNG image allowed",alertVisible:true});
            }
            if(error == "Error: User cancelled image selection"){
                console.log("User cancel image pick");
            }  
        });
    }

    uploadImage() {
        var self = this;
        const uploadData = new FormData();
        var url2 = this.state.ip + "/api/driver-uploadDp/" + this.state.userId;
        uploadData.append('dpImg', {
            uri: this.state.file.path,
            name: 'daus.jpg',
            type: this.state.file.mime,
            userId: this.state.userId,
        });

        axios({
            url: url2,
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
            },
            method: 'POST',
            data: uploadData,
        }).then(function(response){
            self.setState({
                dpImage: self.state.ip + "/" + response.data[0].content.dpPath,
                userDetail: response.data[0].content,
            });
            self.updateUser();
        }).catch(function (error) {
            console.log(error);
        })
    }

    tryUpdateInfo() {
        var self = this;
        axios.post(this.state.ip + "/api/driver-updateInfo/" + this.state.userId, {
            title: self.state.modalTitle,
            input: self.state.modalInput,
        }).then(function (response) {
            self.setState({ modalVisible: false });
            self.updateUser();
        }).catch(function (error) {
            console.log(error);
        });
    }

    componentDidMount(){
        var self = this;

        this.onFocus = this.props.navigation.addListener('focus', () => {
            self.updateUser();
        });
        this.onBlur = this.props.navigation.addListener('blur', () => {
            // self.setState({ dpImage: "" });
        });
    }

    componentWillUnmount(){
        this.onFocus();
        this.onBlur();
    }

    render() {
        var self = this;
        // this.onFocus = this.props.navigation.addListener('focus', () => {
        //     self.updateUser();
        // });
       
        let inputTest;
        if (this.state.modalTitle == "First Name") {
            inputTest = <Input
                placeholder={this.state.userDetail.first_name}
                placeholderTextColor="black"
                style={{ color: 'black' }}
                onChangeText={(modalInput) => this.setState({ modalInput })} />
        }
        if (this.state.modalTitle == "Last Name") {
            inputTest = <Input
                placeholder={this.state.userDetail.last_name}
                placeholderTextColor="black"
                style={{ color: 'black' }}
                onChangeText={(modalInput) => this.setState({ modalInput })} />
        }
        if (this.state.modalTitle == "Email") {
            inputTest = <Input
                placeholder={this.state.userDetail.email}
                placeholderTextColor="black"
                style={{ color: 'black' }}
                onChangeText={(modalInput) => this.setState({ modalInput })} />
        }
        if (this.state.modalTitle == "Phone") {
            inputTest = <Input
                placeholder={this.state.userDetail.phone}
                placeholderTextColor="black"
                keyboardType='number-pad'
                style={{ color: 'black' }}
                onChangeText={(modalInput) => this.setState({ modalInput })} />
        }

        return (
            <ScrollView>
                <View style={styles.container}>

                    {/* This is modal for updating info */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                    >
                        <View style={styles.modalContainer}>
                            <View style={styles.modalContent}>
                                <Text style={styles.modalTitle}>{this.state.modalTitle}</Text>
                                <Item style={styles.inputContainer}>
                                    {inputTest}
                                </Item>
                                <View style={styles.logoutButtonContainer}>
                                    <TouchableOpacity style={styles.logoutNoButton} onPress={() => this.setState({ modalVisible: false })}>
                                        <Text style={{ color: "white", fontWeight: "bold" }}>Cancel</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.logoutYesButton} onPress={() => this.tryUpdateInfo()}>
                                        <Text style={{ color: "white", fontWeight: "bold" }}>Submit</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                    {/* This is modal for alert */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.alertVisible}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.modalContent}>
                            <View
                                style={{
                                    borderBottomColor: "rgb(251,82,87)",
                                    borderBottomWidth: 2,
                                    borderStyle: 'solid',
                                    marginBottom: 20,
                                }}
                            >
                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        fontSize: 24,
                                        color: "rgb(251,82,87)",
                                        marginBottom: 5,
                                    }}>ALERT</Text>
                            </View>

                            <Text>{this.state.alertMsg}</Text>

                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ alertVisible: false });
                                }}
                                style={styles.hideModalButton}>

                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        color: 'white',
                                    }}
                                >Close</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                    <View style={styles.dpContainer}>
                        {/* This is back button */}
                        <TouchableOpacity style={styles.chevronContainer} onPress={() => this.goBackScreen()}>
                            <Image style={styles.chevronLeft} source={require('../assets/chevron_left.png')} />
                        </TouchableOpacity>

                        <FastImage style={styles.dpImage} source={{ uri: this.state.dpImage, cache: FastImage.cacheControl.web, priority: FastImage.priority.normal, }} />
                        <TouchableOpacity style={styles.changeDpContainer} onPress={() => this.selectImage()}>
                            <Image style={styles.cameraIcon} source={require('../assets/camera.png')} />
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>Change picture</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.linkContainer}>
                        <Text style={styles.categoryHeader}>Edit Profile</Text>
                        {/* This is first name  */}
                        <View style={styles.linkHolder}>
                            <Text style={styles.linkTitle}>First Name</Text>
                            <TouchableOpacity style={styles.linkTouch} onPress={() => this.openModal('First Name')}>
                                <Text style={styles.linkContent}>{this.state.userDetail.first_name}</Text>
                                <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
                            </TouchableOpacity>
                        </View>

                        {/* This is last name */}
                        <View style={styles.linkHolder}>
                            <Text style={styles.linkTitle}>Last Name</Text>
                            <TouchableOpacity style={styles.linkTouch} onPress={() => this.openModal('Last Name')}>
                                <Text style={styles.linkContent}>{this.state.userDetail.last_name}</Text>
                                <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
                            </TouchableOpacity>
                        </View>

                        {/* This is email */}
                        <View style={styles.linkHolder}>
                            <Text style={styles.linkTitle}>Email</Text>
                            <TouchableOpacity style={styles.linkTouch} onPress={() => this.openModal('Email')}>
                                <Text style={styles.linkContent}>{this.state.userDetail.email}</Text>
                                <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
                            </TouchableOpacity>
                        </View>

                        {/* This is phone */}
                        <View style={styles.linkHolder}>
                            <Text style={styles.linkTitle}>Phone</Text>
                            <TouchableOpacity style={styles.linkTouch} onPress={() => this.openModal('Phone')}>
                                <Text style={styles.linkContent}>{this.state.userDetail.phone}</Text>
                                <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
                            </TouchableOpacity>
                        </View>

                        {/* This is gender */}
                        <View style={styles.linkHolder}>
                            <Text style={styles.linkTitle}>Gender</Text>
                            <TouchableOpacity style={styles.linkTouch}>
                                <Text style={styles.linkContent}></Text>
                                <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    dpContainer: {
        backgroundColor: "rgb(251,82,87)",
        width: screenWidth,
        height: 250,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 20,
    },
    dpImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 5,
        borderColor: 'white',
        marginBottom: 10,
    },
    firstName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 24,
    },
    lastName: {
        color: 'white',
    },
    categoryHeader: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
        fontSize: 22,
        marginBottom: 30,
        marginTop: 20,
    },
    linkContainer: {
        paddingLeft: 20,
    },
    linkHolder: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40,
    },
    linkTitle: {
        fontWeight: 'bold',
        fontSize: 16,
        marginRight: 10,
    },
    linkContent: {
        position: 'absolute',
        right: 60,
    },
    chevronRight: {
        width: 20,
        height: 20,
        position: 'absolute',
        right: 20,
        opacity: 0.6,
    },
    linkTouch: {
        position: 'absolute',
        right: 10,
        width: 350,
        height: 30,
        justifyContent: 'center',
    },
    changeDpContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent:'center',
    },
    cameraIcon: {
        width: 20,
        height: 20,
        marginRight: 10,
    },
    chevronContainer: {
        position: 'absolute',
        left: 20,
        top: 20,
    },
    chevronLeft: {
        width: 30,
        height: 30,
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: 24,
        color: "rgb(251,82,87)",
        marginBottom: 0,
    },
    inputContainer: {
        marginTop: 30,
        height: 30,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
})
