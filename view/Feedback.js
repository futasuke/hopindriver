import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    Form,
    Textarea
} from 'native-base';
import 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import axios from 'axios';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class Feedback extends React.Component {
    state = {
        userDetail: this.props.route.params.userDetail,
        ip: this.props.route.params.ip,
        title: "",
        description: "",
        suggestion: "",
        showSpinner: false,
        modalVisible: false,
        alertMsg: "",
    }

    submitForm() {
        var self = this;
        this.setState({ showSpinner: true });

        axios.post(self.state.ip + '/api/try-submitFeedback', {
            user_id: self.state.userDetail.id,
            title: self.state.title,
            description: self.state.description,
            suggestion: self.state.suggestion,
        }).then(function (response) {
            console.log(response);
            self.setState({alertMsg:response.data[0].message});
            self.setState({ showSpinner: false });
            self.showAlert();
        }).catch(function (error) {
            console.log(error);
            self.setState({ showSpinner: false });
        });
    }

    showAlert(){
        this.setState({modalVisible:true});    
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>

                    {/* This is for ActivityIndicator aka Spinner */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.showSpinner}
                    >
                        <View style={styles.spinnerContainer}>
                            <View style={styles.spinnerModalContent}>
                                <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                            </View>
                        </View>
                    </Modal>

                    {/* This is modal for alert */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                    >
                        <View style={styles.modalContainer}>
                            <View style={styles.modalContent}>
                                <View
                                    style={{
                                        borderBottomColor: "rgb(251,82,87)",
                                        borderBottomWidth: 2,
                                        borderStyle: 'solid',
                                        marginBottom: 20,
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            fontSize: 24,
                                            color: "rgb(251,82,87)",
                                            marginBottom: 5,
                                        }}>ALERT</Text>
                                </View>

                                <Text>{this.state.alertMsg}</Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ modalVisible: false });
                                        this.props.navigation.navigate('Setting');
                                    }}
                                    style={styles.hideModalButton}>

                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            color: 'white',
                                        }}
                                    >Close</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>


                    <View style={styles.header}>
                        <TouchableOpacity style={styles.chevronContainer} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../assets/chevron_left.png')} style={styles.chevronLeft} />
                        </TouchableOpacity>
                        <Text style={styles.headerFont}>Feedback</Text>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Image source={require('../assets/sample_banner.png')} style={{ width: screenWidth - 20, height: 150, borderRadius: 20 }}></Image>
                    </View>
                    <View style={{ alignSelf: "baseline", marginLeft: 15, marginTop: 20 }}>
                        <View>
                            <Text style={styles.title}>Title</Text>
                            <Item rounded style={styles.inputContainer}>
                                <Input
                                    placeholder="Email"
                                    placeholderTextColor="white"
                                    style={{ color: 'black' }}
                                    keyboardType="email-address"
                                    onChangeText={(title) => this.setState({ title })} />
                            </Item>
                        </View>
                        <View>
                            <Text style={styles.title}>Description</Text>
                            <Form>
                                <Textarea rowSpan={5} bordered style={{ borderRadius: 20 }} onChangeText={(description) => this.setState({ description })} />
                            </Form>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.title}>Suggestion</Text>
                            <Form>
                                <Textarea rowSpan={5} bordered style={{ borderRadius: 20 }} onChangeText={(suggestion) => this.setState({ suggestion: suggestion })} />
                            </Form>
                        </View>

                        <TouchableOpacity style={styles.submitButton} onPress={() => this.submitForm()}>
                            <Text style={styles.submitButtonLabel}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        // backgroundColor: "rgb(251,82,87)",
    },
    header: {
        backgroundColor: "white",
        width: screenWidth,
        height: 50,
        justifyContent: "center",
        backgroundColor: "rgb(251,82,87)",
        flexDirection: "row",
    },
    headerFont: {
        fontWeight: "bold",
        alignSelf: "center",
        fontSize: 24,
        color: "white",
    },
    chevronContainer: {
        alignSelf: "center",
        position: 'absolute',
        left: 20,
    },
    chevronLeft: {
        width: 20,
        height: 20,
    },
    inputContainer: {
        width: screenWidth - 30,
        paddingLeft: 10,
        backgroundColor: "white",
        marginBottom: 30,
        height: 45,
    },
    title: {
        color: "rgb(251,82,87)",
        fontWeight: "bold",
        fontSize: 22,
        marginBottom: 10,
    },
    submitButton: {
        backgroundColor: "rgb(251,82,87)",
        width: screenWidth - 30,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginTop: 30,
    },
    submitButtonLabel: {
        color: "white",
        fontWeight: 'bold',
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
})