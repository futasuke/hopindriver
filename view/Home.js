import React, { Component } from 'react';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    FlatList,
} from 'react-native';

import 'react-native-gesture-handler';
import Geolocation from 'react-native-geolocation-service';
import { SliderBox } from "react-native-image-slider-box";
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import { NavigationEvents, DrawerActions } from '@react-navigation/native';
import FactCourasel from '../component/FactCourasel';
import HomeBody from '../component/HomeBody';
import io from "socket.io-client";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use location');
        } else {
            console.log('Location permission denied');
        }
    } catch (err) {
        console.warn(err);
    }
}

export default class HomeScreen extends Component {
    state = {
        userId: this.props.route.params.userId,
        ip: this.props.route.params.ip,
        userDetail: this.props.route.params.userDetail,
        coord: '',
        images: [{ 'image': require('../assets/banner_fact_hopin.png'), id: 1 }, { image: require('../assets/banner_advertise.png'), id: 2 }],
        newsCarousel: [],
    }

    goToMainMap() {
        var self = this;
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(function (response) {
            if (response == true) {
                Geolocation.getCurrentPosition(
                    (position) => {
                        self.setState({ coord: position.coords }, function () {
                            this.props.navigation.navigate('GoMap', {
                                coord: this.state.coord,
                                ip: this.state.ip,
                                userDetail: this.state.userDetail,
                            });
                        });
                    },
                    (error) => {
                        // See error code charts below.
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        });

        // this.props.navigation.navigate('GoMap', {
        //     coord: this.state.coord,
        //     ip: this.state.ip,
        //     userDetail: this.state.userDetail,
        // });
    }

    componentDidMount() {
        var test = requestLocationPermission();
        var self = this;
    }

    render() {
        var self = this;

        return (
            <SafeAreaView>
                <ImageBackground source={require('../assets/homebg.jpg')} style={{ width: screenWidth, height: screenHeight }}>
                    <View style={styles.container}>
                        <View style={styles.topBgStlye}></View>

                        {/* Menu button */}
                        <TouchableOpacity style={styles.menuIconContainer} onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}>
                            <Image source={require('../assets/menu-nobg.png')} style={styles.menuIcon} />
                        </TouchableOpacity>

                        {/* This is welcome card */}
                        <View style={styles.welcomeCard}>
                            <Image source={require('../assets/logoSmall.png')} style={styles.logoWelcome}></Image>
                            <View style={styles.welcomeText}>
                                <Text style={styles.welcomeTitle}>Welcome to Hopin</Text>
                                <View style={styles.separator}></View>
                                <Text style={styles.welcomeDesc}>Get a ride easily and affordable!</Text>
                            </View>
                        </View>

                        {/* This is function card */}
                        <View style={styles.functionCard}>
                            {/* This is normal car */}
                            <TouchableOpacity style={styles.functionContainer} onPress={() => this.goToMainMap()}>
                                <Image source={require('../assets/car.png')} style={styles.functionIcon} />
                                <Text>Normal</Text>
                            </TouchableOpacity>
                            {/* This is carpool car */}
                            <TouchableOpacity style={styles.functionContainer}>
                                <Image source={require('../assets/carpool.png')} style={styles.functionIcon} />
                                <Text>Topup</Text>
                            </TouchableOpacity>
                            {/* This is ride history */}
                            <TouchableOpacity style={styles.functionContainer}>
                                <Image source={require('../assets/history.png')} style={styles.functionIcon} />
                                <Text>History</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 700, backgroundColor: "rgb(254, 225, 225)" }}>
                            {/* <View style={{backgroundColor: "rgb(254, 225, 225)",height:10}}></View> */}
                            <HomeBody route={this.props.route} navigation={this.props.navigation} />
                        </View>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: "rgb(254, 225, 225)",
        width: screenWidth,
        height: screenHeight,
    },
    menuIconContainer: {
        marginLeft: 30,
        marginTop: 10,
        marginBottom: 10,
        width: 40,
        height: 40,
    },
    menuIcon: {
        width: 40,
        height: 40,
    },
    welcomeCard: {
        backgroundColor: 'white',
        width: screenWidth - 50,
        height: 100,
        borderRadius: 20,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        marginBottom: 30,
        alignSelf: "center",
    },
    logoWelcome: {
        width: 70,
        height: 70,
        borderRadius: 15,
        marginRight: 30,
    },
    welcomeTitle: {
        fontWeight: 'bold',
        fontSize: 28,
    },
    welcomeDesc: {
        fontSize: 12,
    },
    separator: {
        backgroundColor: "rgb(251,82,87)",
        height: 2,
        width: 210,
        marginTop: 5,
        marginBottom: 5,
    },
    functionCard: {
        backgroundColor: 'white',
        width: screenWidth - 50,
        height: 70,
        borderRadius: 20,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        // paddingLeft: 20,
        // marginBottom: 20,
        alignSelf: "center",
        zIndex: 9,
    },
    functionIcon: {
        width: 40,
        height: 40,
    },
    functionContainer: {
        alignItems: 'center',
        marginLeft: 70,
    },
    newsCard: {
        backgroundColor: 'white',
        width: screenWidth - 50,
        height: 300,
        borderRadius: 20,
        marginTop: 20,
        // flexDirection: 'row',
        paddingTop: 10,
        paddingLeft: 20,
        // alignItems: 'center',
        alignSelf: "center",
        // marginBottom:20,
    },
    cardTitle: {
        fontWeight: 'bold',
        fontSize: 22,
        color: "rgb(251,82,87)",
    },
    newsContainer: {
        // width:screenWidth - 85,
        // height:300,
        // backgroundColor:'black',
        // flex: 1,
        borderColor: 'grey',
        borderWidth: 2,
        borderRadius: 20,
        marginRight: 10,
        marginTop: 15,
        height: 220,
    },
    newsImage: {
        height: 150,
        width: 250,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    newsTitle: {
        fontWeight: 'bold',
    },
    newsContent: {
        marginTop: 10,
        marginLeft: 10,
    },
    newsDescription: {
        marginTop: 10,
    },
    topBgStlye: {
        backgroundColor: "rgb(251,82,87)",
        // backgroundColor:"black",
        width: screenWidth,
        height: 250,
        zIndex: 0,
        position: 'absolute',
        top: 0,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    factAdContainer: {
        borderColor: 'grey',
        borderWidth: 2,
        marginRight: 10,
        marginTop: 15,
        height: 150,
    },
    factAdImage: {
        height: 150,
        width: 400,
    }
})