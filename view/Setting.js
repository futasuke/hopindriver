import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
} from 'react-native';
import 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import { NavigationEvents, DrawerActions } from '@react-navigation/native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class SettingScreen extends React.Component {
  state = {
    userId: this.props.route.params.userId,
    userDetail: this.props.route.params.userDetail,
    ip: this.props.route.params.ip,
    dpImage: this.props.route.params.dpImage,
    focusSubscription: '',
    modalVisible: false,
  }

  updateUser() {
    var self = this;
    axios.get(this.state.ip + '/api/driver-getDriver/' + this.state.userId)
      .then(function (response) {
        self.setState({ userDetail: response.data[0].content });
        self.setState({ dpImage: self.state.ip + "/" + response.data[0].content.dpPath });
      }).catch(function (error) {
        console.log(error);
      });
  }

  goToEditProfile() {
    this.props.navigation.navigate('EditProfile', {
      userId: this.state.userId,
      userDetail: this.state.userDetail,
      ip: this.state.ip,
      dpImage: this.state.dpImage,
    })
  }

  showAlertPassword() {
    this.setState({ modalVisible: true });
  }

  goToFeedback() {
    var self = this;

    this.props.navigation.navigate('Feedback',{
      userDetail:self.state.userDetail,
      ip:self.state.ip,
    });
  }

  componentDidMount() {
    var self = this;

    this.onFocus = this.props.navigation.addListener('focus', () => {
      self.updateUser();
    });
    this.onBlur = this.props.navigation.addListener('blur', () => {
      self.setState({ dpImage: "" });
    });
  }

  componentWillUnmount() {
    this.onFocus();
    this.onBlur();   
  }

  render() {
    var self = this;
    // this.onFocus = this.props.navigation.addListener('focus', () => {
    //       self.updateUser();
    //   });

    return (
      <View style={styles.container}>

        {/* This is modal for change password */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <Text style={styles.modalTitle}>Alert</Text>
              <View>
                <Text style={{ marginTop: 10 }}>Automatic password change handler is not available.
                    Thus, you must manually email to the developer firdaus531@gmail.com if you want to change password.</Text>
                <Text style={{ marginTop: 10 }}>Sorry for the inconvenience.</Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ modalVisible: false });
                }}
                style={styles.hideModalButton}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: 'white',
                  }}
                >Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        {/* This is container for profile thingy */}
        <View style={styles.dpContainer}>
          {/* Menu button */}
          <TouchableOpacity style={styles.menuIconContainer} onPress={()=>this.props.navigation.dispatch(DrawerActions.toggleDrawer())}>
            <Image source={require('../assets/menu-nobg.png')} style={styles.menuIcon}></Image>
          </TouchableOpacity>

          <FastImage style={styles.dpImage} source={{ uri: this.state.dpImage, cache: FastImage.cacheControl.web, priority: FastImage.priority.normal, }} />
          <Text style={styles.firstName}>{this.state.userDetail.first_name}</Text>
          <Text style={styles.lastName}>{this.state.userDetail.last_name}</Text>
        </View>

        {/* This is container for links */}
        <View style={styles.linkContainer}>
          <View>
            <Text style={styles.categoryHeader}>Account setting</Text>
            {/* This is profile link container */}
            <TouchableOpacity style={styles.linkHolder} onPress={() => this.goToEditProfile()}>
              <Image style={styles.linkIcon} source={require('../assets/user.png')} />
              <Text style={styles.linkText}>Edit Profile</Text>
              <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
            </TouchableOpacity>
            {/* This is change password container */}
            <TouchableOpacity style={styles.linkHolder} onPress={() => this.showAlertPassword()}>
              <Image style={styles.linkIcon} source={require('../assets/password.png')} />
              <Text style={styles.linkText}>Change Password</Text>
              <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
            </TouchableOpacity>

            <Text style={styles.categoryHeader}>Support</Text>
            {/* This is contact us container */}
            <TouchableOpacity style={styles.linkHolder} onPress={() => this.props.navigation.navigate('ContactUs')}>
              <Image style={styles.linkIcon} source={require('../assets/contact.png')} />
              <Text style={styles.linkText}>Contact Us</Text>
              <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
            </TouchableOpacity>
            {/* This is feedback container */}
            <TouchableOpacity style={styles.linkHolder} onPress={() => this.goToFeedback()}>
              <Image style={styles.linkIcon} source={require('../assets/feedback.png')} />
              <Text style={styles.linkText}>Feedback</Text>
              <Image style={styles.chevronRight} source={require('../assets/chevron_right.png')} />
            </TouchableOpacity>

            {/* <Text style={styles.categoryHeader}>App setting</Text>
              <Text>Not yet available</Text> */}

            <View>
              <Text style={styles.appInfoTitle}>App Info</Text>
              <Text style={{ color: 'rgba(0,0,0,0.5)' }}>App version : 1.0.0</Text>
              <Text style={{ color: 'rgba(0,0,0,0.5)' }}>Developer : Firdaus Fadzi</Text>
              <Text style={{ color: 'rgba(0,0,0,0.5)' }}>Email : firdaus531@gmail.com</Text>
              <Text style={{ color: 'rgba(0,0,0,0.5)' }}>Instagram : @firdausfdz_</Text>
              <Text style={{ color: 'rgba(0,0,0,0.5)' }}>Twitter : @damnus98</Text>
            </View>
          </View>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // alignItems: "center",
    flex: 1,
    // backgroundColor: "rgb(251,82,87)",
  },
  dpContainer: {
    backgroundColor: "rgb(251,82,87)",
    width: screenWidth,
    height: 250,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
  },
  dpImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 5,
    borderColor: 'white',
    marginBottom: 10,
  },
  firstName: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  lastName: {
    color: 'white',
  },
  linkContainer: {
    paddingLeft: 20,
  },
  categoryHeader: {
    color: "rgb(251,82,87)",
    fontWeight: 'bold',
    fontSize: 22,
    marginBottom: 20,
    marginTop: 20,
  },
  linkHolder: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    // borderColor:'black',
    // borderWidth:1,
  },
  linkIcon: {
    height: 25,
    width: 25,
    marginRight: 20,
    opacity: 0.6,
  },
  linkText: {
    fontWeight: 'bold',
    color: 'rgba(0,0,0,0.7)',
  },
  chevronRight: {
    width: 20,
    height: 20,
    position: 'absolute',
    right: 20,
    opacity: 0.6,
  },
  chevronContainer: {
    position: 'absolute',
    left: 20,
    top: 20,
  },
  chevronLeft: {
    width: 30,
    height: 30,
  },
  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexDirection: 'column',
  },
  modalContent: {
    width: 300,
    height: 200,
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 20,
  },
  modalTitle: {
    fontWeight: 'bold',
    fontSize: 24,
    color: "rgb(251,82,87)",
    marginBottom: 0,
  },
  hideModalButton: {
    position: 'absolute',
    bottom: 10,
    right: 20,
    backgroundColor: "rgb(251,82,87)",
    height: 30,
    width: 60,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  appInfoTitle: {
    color: "rgb(251,82,87)",
    fontWeight: 'bold',
    fontSize: 22,
    marginBottom: 10,
    marginTop: 20,
  },
  menuIconContainer:{
    position: 'absolute',
    left: 20,
    top: 20,
  },
  menuIcon:{
    width:40,
    height:40,
  },
})