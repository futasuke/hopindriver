import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    H2,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import { StackActions, NavigationActions, BaseRouter } from '@react-navigation/native';
// import FirstDetail from './FirstDetail';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class RegisterScreen extends React.Component {
    state = {
        email: '',
        password: '',
        retype: '',
        ip: this.props.route.params.ip,
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: '',
        userDetail: [],
    }

    goToLogin() {
        this.props.navigation.navigate('Login');
    }

    checkInput() {
        if (this.state.email != '') {
            if (this.state.password != '') {
                if (this.state.retype != '') {
                    this.tryRegister();
                }
                else {
                    this.setState({ alertMsg: 'Please retype password' });
                    this.setState({ modalVisible: true });
                }
            }
            else {
                this.setState({ alertMsg: 'Please enter password' });
                this.setState({ modalVisible: true });
            }
        }
        else {
            this.setState({ alertMsg: 'Please enter email' });
            this.setState({ modalVisible: true });
        }
    }

    tryRegister() {
        var self = this;
        this.setState({ showSpinner: true });

        axios.post(self.state.ip + '/api/driver-register', {
            email: self.state.email,
            password: self.state.password,
            retype: self.state.retype,
        }).then(function (response) {
            if (response.data[0].message == "Success") {
                self.setState({ userId: response.data[0].content.id })
                self.setState({ userDetail: response.data[0].content })
                self.setState({ showSpinner: false });
                self.props.navigation.dispatch(StackActions.replace('DriverRegister', {
                    userId: self.state.userId,
                    userDetail: self.state.userDetail,
                    ip: self.state.ip,
                }));
            }
            else {
                self.setState({ showSpinner: false });
                self.setState({ alertMsg: response.data[0].message });
                self.setState({ modalVisible: true });
            }
        }).catch(function (error) {
            console.log(error);
            self.setState({ showSpinner: false });
        });
    }


    render() {
        return (
            <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
                <ScrollView>
                    {/* <StatusBar hidden={true} /> */}
                    <View style={styles.container}>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>


                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>


                        {/* This is for title */}
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleLabel}>Create an Account</Text>
                        </View>

                        {/* This is for email */}
                        <Item rounded style={styles.emailContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/email.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Email"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType="email-address"
                                onChangeText={(email) => this.setState({ email })} />
                        </Item>

                        {/* This is for password */}
                        <Item rounded style={styles.passwordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ password })} />
                        </Item>

                        {/* This is retype password */}
                        <Item rounded style={styles.repasswordContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/password.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Retype Password"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                secureTextEntry={true}
                                onChangeText={(retype) => this.setState({ retype })} />
                        </Item>

                        {/* This is register button */}
                        <TouchableOpacity style={styles.registerButton} onPress={() => this.checkInput()}>
                            <Text style={styles.buttonLabel}>Register</Text>
                        </TouchableOpacity>

                        {/* This is back button */}
                        <TouchableOpacity style={styles.backButton} onPress={() => this.goToLogin()}>
                            <Text style={styles.backButtonLabel}>Back</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </ImageBackground >
        );
    }
}


const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
    },
    container: {
        // justifyContent:"center",
        marginTop: 100,
        alignItems: "center",
        flex: 1,
    },
    titleContainer: {
        borderBottomColor: "rgb(251,82,87)",
        borderBottomWidth: 2,
        marginBottom: 50,
    },
    titleLabel: {
        fontSize: 32,
        fontFamily: 'Aria',
        fontWeight: 'bold',
        color: "rgb(251,82,87)",
    },
    emailContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    passwordContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    repasswordContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 60,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 33,
        height: 25,
    },
    registerButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 40,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    backButton: {
        backgroundColor: "rgb(251,82,87)",
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 60,
    },
    backButtonLabel: {
        color: "white",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    }
})