import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    H2,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import  { StackActions, NavigationActions, BaseRouter } from '@react-navigation/native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class FirstDetail extends React.Component{
    state={
        fname:'',
        lname:'',
        phone:'',
        ip: this.props.route.params.ip,
        modalVisible: false,
        alertMsg: '',
        showSpinner: false,
        userId: this.props.route.params.userId,
    }

    checkInput(){
        if(this.state.fname!=''){
            if(this.state.lname!=''){
                if(this.state.phone!=''){
                    this.storeDetail();
                }else{
                    this.setState({alertMsg:'Please enter phone number', modalVisible:true});
                }
            }else{
                this.setState({alertMsg:'Please enter last name', modalVisible:true});
            }
        }else{
            this.setState({alertMsg:'Please enter first name', modalVisible:true});
        }
    }

    storeDetail(){
        var self = this;
        this.setState({showSpinner:true});
        axios.post(self.state.ip+'/api/driver-firstdetail',{
            userId:self.state.userId,
            fname:self.state.fname,
            lname:self.state.lname,
            phone:self.state.phone,
        }).then(function(response){
            self.setState({showSpinner:false});
            self.props.navigation.dispatch(StackActions.replace('HomeDrawer',{
                userId: self.state.userId,
                userDetail: response.data[0].content,
                dpImage: self.state.ip+"/"+response.data[0].content.dpPath,
                ip:self.state.ip,
            }));
        }).catch(function(error){
            self.setState({showSpinner:false});
            console.log(error);
        })
    }

    render(){
        return(
            <ScrollView>
                <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
                    <StatusBar hidden={true} />
                    <View style={styles.container}>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)"/>
                                </View>                                                      
                            </View>
                        </Modal>

                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor:"rgb(251,82,87)",
                                            borderBottomWidth:2,
                                            borderStyle:'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom:5,                                  
                                            }}>ALERT</Text>  
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>

                         {/* This is for title */}
                         <View style={styles.titleContainer}>
                            <Text style={styles.titleLabel}>Enter your first detail</Text>
                        </View>

                         {/* This is for first name */}
                         <Item rounded style={styles.firstNameContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/user.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="First Name"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                onChangeText={(fname) => this.setState({ fname })} />
                        </Item>

                        {/* This is for last name */}
                        <Item rounded style={styles.lastNameContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/user.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Last Name"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                onChangeText={(lname) => this.setState({ lname })} />
                        </Item>

                        {/* This is for phone */}
                        <Item rounded style={styles.phoneContainer}>
                            <View style={styles.iconContainer}>
                                <Image source={require('../assets/phone.png')} style={styles.inputIcon}></Image>
                            </View>
                            <Input
                                placeholder="Phone Number"
                                placeholderTextColor="white"
                                style={{ color: 'white' }}
                                keyboardType='number-pad'
                                onChangeText={(phone) => this.setState({ phone })} />
                        </Item>

                        {/* This is next button */}
                        <TouchableOpacity style={styles.nextButton} onPress={()=>this.checkInput()}>
                            <Text style={styles.buttonLabel}>Next</Text>
                        </TouchableOpacity>

                    </View>
                </ImageBackground>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bgimage:{
        width: screenWidth,
        height: screenHeight,
    },
    container: {
        // justifyContent:"center",
        marginTop: 150,
        alignItems: "center",
        flex: 1,
    },
    titleContainer: {
        borderBottomColor: 'white',
        borderBottomWidth: 2,
        marginBottom: 50,
    },
    titleLabel: {
        fontSize: 32,
        fontFamily: 'Aria',
        fontWeight: 'bold',
        color: 'white',
    },
    firstNameContainer:{
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 33,
        height: 25,
    },
    lastNameContainer:{
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 30,
    },
    phoneContainer:{
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 60,
    },
    nextButton:{
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 40,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer:{
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent:{
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    }
})