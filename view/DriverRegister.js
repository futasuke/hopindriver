import React from 'react';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
    H2,
} from 'native-base';

import axios from 'axios';
import FastImage from 'react-native-fast-image';
import ImagePicker, { clean } from 'react-native-image-crop-picker';
import  { StackActions, NavigationActions, BaseRouter } from '@react-navigation/native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class DriverRegisterScreen extends React.Component {
    state = {
        ip: this.props.route.params.ip,
        modalVisible: true,
        modalUploadVisible:false,
        alertMsg: 'If you accidentically press "back", just login. It will prompt this driver registration again.',
        showSpinner: false,
        userId:this.props.route.params.userId,
        userDetail: this.props.route.params.userDetail,
        fileFrontIc: '',
        fileBackIc: '',
        fileLicense: '',
        fileCarImg: '',
        frontIcName: '',
        backIcName: '',
        licenseName: '',
        carImgName: '',
    }

    componentDidMount(){
        console.log(this.state.ip);
    }

    selectImage(imgType) {
        var self = this;

        ImagePicker.openCamera({
            width: 500,
            height: 500,
            cropping: true,
            mediaType: 'photo',
            includeBase64: true,
        }).then(image => {
            if (imgType == 'frontic') {
                self.setState({ fileFrontIc: image });
                var filepath = this.state.fileFrontIc.path;
                var filename = filepath.substring(filepath.lastIndexOf('/') + 1);
                self.setState({ frontIcName: filename });
            }
            if (imgType == 'backic') {
                self.setState({ fileBackIc: image });
                var filepath = this.state.fileBackIc.path;
                var filename = filepath.substring(filepath.lastIndexOf('/') + 1);
                self.setState({ backIcName: filename });
            }
            if (imgType == 'driverlicense') {
                self.setState({ fileLicense: image });
                var filepath = this.state.fileLicense.path;
                var filename = filepath.substring(filepath.lastIndexOf('/') + 1);
                self.setState({ licenseName: filename });
            }
            if (imgType == 'frontcar') {
                self.setState({ fileCarImg: image });
                var filepath = this.state.fileCarImg.path;
                var filename = filepath.substring(filepath.lastIndexOf('/') + 1);
                self.setState({ carImgName: filename });
            }
        });
    }

    uploadImage() {
        var self = this;
        this.setState({showSpinner:true});
        
        if (this.state.frontIcName != '' && this.state.backIcName != '' && this.state.licenseName != '' && this.state.carImgName != '') {
            const frontIcData = new FormData();
            const backIcData = new FormData();
            const licenseData = new FormData();
            const carImgData = new FormData();
            var url = this.state.ip + "/api/driver-uploadImage/" + this.state.userId;
            console.log(url);

            frontIcData.append('frontIcImg', {
                uri: this.state.fileFrontIc.path,
                name: 'daus.jpg',
                type: this.state.fileFrontIc.mime,
                userId: this.state.userId,
            });

            backIcData.append('backIcImg', {
                uri: this.state.fileBackIc.path,
                name: 'daus.jpg',
                type: this.state.fileBackIc.mime,
                userId: this.state.userId,
            });

            licenseData.append('licenseImg', {
                uri: this.state.fileLicense.path,
                name: 'daus.jpg',
                type: this.state.fileLicense.mime,
                userId: this.state.userId,
            });

            carImgData.append('carImg', {
                uri: this.state.fileCarImg.path,
                name: 'daus.jpg',
                type: this.state.fileCarImg.mime,
                userId: this.state.userId,
            });

            axios({
                url: url+"/frontic",
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                },
                method: 'POST',
                data: frontIcData,
            }).then(function (response) {
                console.log(response);      
            }).catch(function (error) {
                console.log(error);
                self.setState({showSpinner:false});
            });

            axios({
                url: url+"/backic",
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                },
                method: 'POST',
                data: backIcData,
            }).then(function (response) {
                console.log(response);
            }).catch(function (error) {
                console.log(error);
                self.setState({showSpinner:false});
            });
           
            axios({
                url: url+"/license",
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                },
                method: 'POST',
                data: licenseData,
            }).then(function (response) {
                console.log(response);
            }).catch(function (error) {
                console.log(error);
                self.setState({showSpinner:false});
            });

            axios({
                url: url+"/carimg",
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                },
                method: 'POST',
                data: carImgData,
            }).then(function (response) {
                console.log(response);
                self.setState({showSpinner:false});
                self.uploadSuccess();
            }).catch(function (error) {
                console.log(error);
                self.setState({showSpinner:false});
            });

        }
        else{
            self.setState({modalVisible: true , alertMsg:'Please upload all required images.' , showSpinner:false});
        }
    }

    uploadSuccess(){
        this.setState({alertMsg:'Your images have been successfully uploaded. Please wait for approval in around 2 or 3 working days. We will email it to you.'});
        this.setState({modalUploadVisible:true});
    }

    redirectLogin(){
        this.setState({modalUploadVisible:false});
        this.props.navigation.navigate('Login');
    }

    // componentDidMount(){
    //     console.log(this.state.userDetail);
    // }

    render() {
        let frontIcLabel = 'Upload Front IC Image';
        let backIcLabel = 'Upload Back IC Image';
        let licenseLabel = 'Upload Driver License Image';
        let carImgLabel = 'Upload Front Car Image';

        if (this.state.frontIcName != '') {
            frontIcLabel = this.state.frontIcName;
        }
        if (this.state.backIcName != '') {
            backIcLabel = this.state.backIcName;
        }
        if (this.state.licenseName != '') {
            licenseLabel = this.state.licenseName;
        }
        if (this.state.carImgName != '') {
            carImgLabel = this.state.carImgName;
        }


        return (
            <ScrollView>
                <ImageBackground source={require('../assets/loginBg.png')} style={styles.bgimage}>
                    {/* <StatusBar hidden={true} /> */}
                    <View style={styles.container}>

                        {/* This is for ActivityIndicator aka Spinner */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showSpinner}
                        >
                            <View style={styles.spinnerContainer}>
                                <View style={styles.spinnerModalContent}>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                                </View>
                            </View>
                        </Modal>

                        {/* This is modal for upload success */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalUploadVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => this.redirectLogin()}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>


                        {/* This is modal for alert */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    <View
                                        style={{
                                            borderBottomColor: "rgb(251,82,87)",
                                            borderBottomWidth: 2,
                                            borderStyle: 'solid',
                                            marginBottom: 20,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                fontSize: 24,
                                                color: "rgb(251,82,87)",
                                                marginBottom: 5,
                                            }}>ALERT</Text>
                                    </View>

                                    <Text>{this.state.alertMsg}</Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}
                                        style={styles.hideModalButton}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                            }}
                                        >Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>


                        {/* This is for title */}
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleLabel}>Driver Registration</Text>
                        </View>

                        <View style={styles.icDocContainer}>
                            <TouchableOpacity style={styles.changeDpContainer} onPress={() => this.selectImage('frontic')}>
                                <Image style={styles.cameraIcon} source={require('../assets/camera.png')} />
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{frontIcLabel}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.icDocContainer}>
                            <TouchableOpacity style={styles.changeDpContainer} onPress={() => this.selectImage('backic')}>
                                <Image style={styles.cameraIcon} source={require('../assets/camera.png')} />
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{backIcLabel}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.driverLicenseContainer}>
                            <TouchableOpacity style={styles.changeDpContainer} onPress={() => this.selectImage('driverlicense')}>
                                <Image style={styles.cameraIcon} source={require('../assets/camera.png')} />
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{licenseLabel}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.driverLicenseContainer}>
                            <TouchableOpacity style={styles.changeDpContainer} onPress={() => this.selectImage('frontcar')}>
                                <Image style={styles.cameraIcon} source={require('../assets/camera.png')} />
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{carImgLabel}</Text>
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={styles.registerButton} onPress={() => this.uploadImage()}>
                            <Text style={styles.buttonLabel}>Submit Image</Text>
                        </TouchableOpacity>

                    </View>
                </ImageBackground>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bgimage: {
        width: screenWidth,
        height: screenHeight,
    },
    container: {
        // justifyContent:"center",
        marginTop: 100,
        alignItems: "center",
        flex: 1,
    },
    titleContainer: {
        borderBottomColor: 'white',
        borderBottomWidth: 2,
        marginBottom: 50,
    },
    titleLabel: {
        fontSize: 32,
        fontFamily: 'Aria',
        fontWeight: 'bold',
        color: 'white',
    },
    icDocContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        borderRadius: 20,
        flexDirection: 'row',
        marginBottom: 30,
        height: 50,
        paddingLeft: 20,
    },
    driverLicenseContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        borderRadius: 20,
        flexDirection: 'row',
        marginBottom: 30,
        height: 50,
        paddingLeft: 20,
    },
    repasswordContainer: {
        width: screenWidth - 100,
        // paddingLeft:20,
        backgroundColor: "rgba(0,0,0,0.2)",
        marginBottom: 60,
    },
    iconContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: 60,
        height: 51,
        alignItems: "center",
        justifyContent: "center",
    },
    inputIcon: {
        width: 33,
        height: 25,
    },
    registerButton: {
        backgroundColor: 'white',
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 40,
    },
    buttonLabel: {
        color: "rgb(251,82,87)",
        fontWeight: 'bold',
    },
    backButton: {
        backgroundColor: "rgb(251,82,87)",
        width: screenWidth - 100,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        borderRadius: 30,
        marginBottom: 60,
    },
    backButtonLabel: {
        color: "white",
        fontWeight: 'bold',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: 300,
        height: 200,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    hideModalButton: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        width: 60,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    cameraIcon: {
        width: 20,
        height: 20,
        marginRight: 10,
    },
    changeDpContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent:'center',
    },
});