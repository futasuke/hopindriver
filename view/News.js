import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class News extends React.Component {
    state = {
        news: this.props.route.params.news,
        ip: this.props.route.params.ip,
    }

    render() {
        var self = this;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.chevronContainer} onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('../assets/chevron_left.png')} style={styles.chevronLeft} />
                    </TouchableOpacity>
                    <Text style={styles.headerFont}>News!</Text>
                </View>
                <View style={{ marginTop: 10 }}>
                    <Image source={{ uri: self.state.ip +"/"+ self.state.news.picPath, cache: FastImage.cacheControl.web, priority: FastImage.priority.normal, }} style={{ width: screenWidth - 20, height: 150, borderRadius: 20 }}></Image>
                </View>
                <View style={{ alignSelf: "baseline", marginLeft: 15, marginTop: 20 }}>
                    <Text style={styles.newsTitle}>{this.state.news.title}</Text>
                    <Text>{this.state.news.description}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        // backgroundColor: "rgb(251,82,87)",
    },
    header: {
        backgroundColor: "white",
        width: screenWidth,
        height: 50,
        justifyContent: "center",
        backgroundColor: "rgb(251,82,87)",
        flexDirection: "row",
    },
    headerFont: {
        fontWeight: "bold",
        alignSelf: "center",
        fontSize: 24,
        color: "white",
    },
    chevronContainer: {
        alignSelf: "center",
        position: 'absolute',
        left: 20,
    },
    chevronLeft: {
        width: 20,
        height: 20,
    },
    newsTitle:{
        fontWeight:"bold",
        fontSize:18,
        marginBottom:20,
    }
})