import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Modal,
    PermissionsAndroid,
    FlatList,
    ActivityIndicator,
} from 'react-native';

import {
    Input,
    Icon,
    Item,
    Label,
    Left,
} from 'native-base';
import 'react-native-gesture-handler';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import { setInterval } from 'core-js';
import Geolocation from 'react-native-geolocation-service';
import { startClock, not } from 'react-native-reanimated';
import { showLocation } from 'react-native-map-link';
import MapViewDirections from 'react-native-maps-directions';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class MainMap extends React.Component {
    state = {
        coord: this.props.route.params.coord,
        currentCoord: this.props.route.params.coord,
        ip: this.props.route.params.ip,
        userDetail: this.props.route.params.userDetail,
        coordinate: new AnimatedRegion({
            latitude: '',
            longitude: '',
        }),
        markerCoord: '',
        destinationName: '',
        currentPlaceName: '',
        driverCoord: '',
        requestArr: [],
        checkArr: false,
        power: 'off',
        serviceText: 'offline',
        showSpinner: false,
        showPriceModal: false,
        tempAccept: '',
        price: '',
        pauseSearch: 'no',
        tempOffer: '',
        showWaitingModal: false,
        waitingState: 'no',
        showHeadUpModal: false,
        showRequestContainer: false,
        showBottomContent: 'none',
        calculateDistance: false,
        distanceAchieve: false,
        showPickUpWarning: false,
        less100m: true,
        showRejectModal: false,
        showEndJobWarning: false,
        whatDirection: 'none',
        tempDestCoord: '',
    }

    sendDriverLocation() {
        var self = this;
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(function (response) {
            if (response == true) {
                Geolocation.getCurrentPosition(
                    (position) => {
                        self.setState({ driverCoord: position.coords });
                    },
                    (error) => {
                        // See error code charts below.
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        });
        console.log(self.state.driverCoord);
        axios.post(this.state.ip + "/api/driver-getCoord/" + this.state.userDetail.id, {
            longitude: self.state.driverCoord.longitude,
            latitude: self.state.driverCoord.latitude,
        }).then(function (response) {

        }).catch(function (error) {
            console.log(error);
        })
    }

    destinationMarker(region) {
        var self = this;
        var lat = region.latitude;
        var lon = region.longitude;
        this.setState({ markerCoord: region });

        this.markerMove(lat, lon);

        // Geocoder.from(lat, lon)
        //     .then(json => {
        //         var addressComponent = json.results[0].formatted_address;
        //         self.setState({ destinationName: addressComponent });
        //     })
        //     .catch(error => console.warn(error));
    }

    markerMove(lat, lon) {
        var lati = lat;
        var long = lon;
        var self = this;

        axios.post(this.state.ip + '/api/driver-sendMarkerCoord/' + this.state.userDetail.id, {
            latitude: lati,
            longitude: long,
        }).then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });
    }

    recenterMap() {
        var self = this;

        var latitude = this.state.currentCoord.latitude;
        var longitude = this.state.currentCoord.longitude;

        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(function (response) {
            if (response == true) {
                Geolocation.getCurrentPosition(
                    (position) => {
                        latitude = position.coords.latitude;
                        longitude = position.coords.longitude;

                    },
                    (error) => {
                        // See error code charts below.
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        });

        this.map.animateToRegion({
            latitude,
            longitude,
            latitudeDelta: 0.007,
            longitudeDelta: 0.007,
        })

    }

    componentDidMount() {
        var self = this;
        // this.onFocus = this.props.navigation.addListener('focus', () => {
        //     self.updateDriverLocation();
        // });
        // Geocoder.init("AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI");
        // Geocoder.from(this.state.currentCoord.latitude, this.state.currentCoord.longitude).then(json => {
        //     self.setState({ currentPlaceName: json.results[0].formatted_address });
        // }).catch(error => console.warn(error));
        this.setState({ markerCoord: this.state.currentCoord })
        // this.onFocus = this.props.navigation.addListener('focus', () => {
        //     self.intervalFunction();
        // });
        this.interval = setInterval(() => this.intervalFunction(), 2500);
    }

    intervalFunction() {
        if (this.state.power == 'on' && this.state.pauseSearch == 'no') {
            this.getNearbyRequest();
        }
        if (this.state.waitingState == 'yes') {
            this.waitingForPassenger();
        }
        // if (this.state.calculateDistance == true) {
        //     this.calculateDriverPassenger();
        // }
        // this.interval = setInterval(() => this.getNearbyRequest(), 2500);
    }

    waitingForPassenger() {
        var self = this;
        axios.post(this.state.ip + '/api/driver-checkOffer', {
            offerId: this.state.tempOffer.id,
            requestId: this.state.tempOffer.request_id,
        }).then(function (respond) {
            console.log(respond);
            if (respond.data[0].message == 'Offer Accepted') {
                self.setState({ showHeadUpModal: true, waitingState: false, showWaitingModal: false, whatDirection: 'passenger' });
            }
            if (respond.data[0].message == 'Offer Rejected') {
                self.setState({ showRejectModal: true, waitingState: false, showWaitingModal: false });
            }
        }).catch(function (error) {
            console.log(error);
        })
    }

    getNearbyRequest() {
        // console.log('getting nearby request');
        // // var lat = this.state.currentCoord.latitude;
        // // var lng = this.state.currentCoord.longitude;
        // console.log(this.state.markerCoord);
        // console.log(lng);
        console.log('getting all request')

        var self = this;

        axios.post(this.state.ip + '/api/driver-checkRequest', {
            lati: this.state.markerCoord.latitude,
            long: this.state.markerCoord.longitude,
        }).then(function (respond) {
            // console.log(respond);
            // console.log(respond.data[0].content.length);
            console.log(respond);
            if (respond.data[0].content.length > self.state.requestArr.length) {
                self.setState({ checkArr: true });
                console.log(self.state.requestArr);
            }
            self.setState({ requestArr: respond.data[0].content })
            self.setState({ showSpinner: false });
            // console.log('bruh')
        }).catch(function (error) {
            console.log(error)
            self.setState({ showSpinner: false });
        });
    }

    // There is no need to calculate distance to show pickup button. Loss is on driver if he misused the pickup button intentionally.
    // calculateDriverPassenger() {
    //     var driverLat = 0;
    //     var driverLng = 0;
    //     var markerLat = this.state.markerCoord.latitude;
    //     var markerLng = this.state.markerCoord.longitude;
    //     var pickupLat = this.state.tempAccept.pickup_lat;
    //     var pickupLng = this.state.tempAccept.pickup_lng;

    //     // THIS CODE IS FOR REAL LIFE LOCATION UPDATE. 
    //     // PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then(function (response) {
    //     //     if (response == true) {
    //     //         Geolocation.getCurrentPosition(
    //     //             (position) => {
    //     //                 // console.log(position.coords.latitude);
    //     //                 driverLat = position.coords.latitude;
    //     //                 driverLng = position.coords.longitude;
    //     //                 var distanceLat = (driverLat * Math.PI / 180) - (pickupLat * Math.PI / 180);
    //     //                 var distanceLon = (driverLng * Math.PI / 180) - (pickupLng * Math.PI / 180);
    //     //                 var a = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) + Math.cos(driverLat * Math.PI / 180) * Math.cos(pickupLat * Math.PI / 180) * Math.sin(distanceLon / 2) * Math.sin(distanceLon / 2);
    //     //                 var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //     //                 var distance = 6371 * c;
    //     //                 console.log(distance);
    //     //             },
    //     //             (error) => {
    //     //                 // See error code charts below.
    //     //                 console.log(error.code, error.message);
    //     //             },
    //     //             { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    //     //         );
    //     //     }
    //     // });

    //     var lat1 = markerLat;
    //     var lat2 = pickupLat;
    //     var lon1 = markerLng;
    //     var lon2 = pickupLng;
    //     const R = 6371e3; // metres
    //     const φ1 = lat1 * Math.PI / 180; // φ, λ in radians
    //     const φ2 = lat2 * Math.PI / 180;
    //     const Δφ = (lat2 - lat1) * Math.PI / 180;
    //     const Δλ = (lon2 - lon1) * Math.PI / 180;

    //     const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    //         Math.cos(φ1) * Math.cos(φ2) *
    //         Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    //     const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    //     const d = R * c; // in metres

    //     console.log(d);
    // }

    componentWillUnmount() {
        var self = this;
        // this.onFocus;
        clearInterval(this.interval);
        // clearInterval(this.interval);
    }

    turnOnService() {
        var self = this;
        this.setState({ power: 'on', serviceText: 'online', showSpinner: true, showRequestContainer: true });
        axios.post(this.state.ip + '/api/driver-turnOnService', {
            driverId: this.state.userDetail.id,
        }).then(function (respond) {
            console.log(respond)
        }).catch(function (error) {
            console.log(error)
            self.setState({ showSpinner: false });
        })
    }

    turnOffService() {
        var self = this;
        this.setState({ power: 'off', serviceText: 'offline', showSpinner: true, showRequestContainer: false });
        this.setState({ requestArr: [], checkArr: false });
        axios.post(this.state.ip + '/api/driver-turnOffService', {
            driverId: this.state.userDetail.id,
        }).then(function (respond) {
            console.log(respond)
            self.setState({ showSpinner: false });
            self.props.navigation.navigate("Home");
        }).catch(function (error) {
            console.log(error)
            self.setState({ showSpinner: false });
        })
    }

    renderRequestCard = ({ item, index }) => {
        var self = this;
        return (
            <View style={styles.requestCardContainer}>
                <View style={{
                    height: 120,
                    width: screenWidth - 100,
                    borderBottomColor: 'black',
                    borderBottomWidth: 1,
                    borderBottomStyle: 'solid',
                    flexDirection: 'row',
                    flex: 1,
                    zIndex: 9,
                }}>
                    <View style={{ width: '50%', flexDirection: 'column', flex: 1, zIndex: 9 }}>
                        <View style={{ height: '50%' }}>
                            <Text>Name</Text>
                            <Text>{item.user.first_name}</Text>
                            <Text>{item.user.last_name}</Text>
                        </View>
                        <View style={{ height: '50%' }}>
                            <Text>Distance</Text>
                        </View>
                    </View>
                    <View style={{ width: '50%', flexDirection: 'column', flex: 1, zIndex: 9 }}>
                        <View style={{ height: '50%' }}>
                            <Text>Pickup</Text>
                        </View>
                        <View style={{ height: '50%' }}>
                            <Text>Destination</Text>
                            <Text>{item.location_cache.description}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ height: 70, width: screenWidth - 100, borderTopColor: 'black', borderTopWidth: 1, borderTopStyle: 'solid', position: 'relative' }}>
                    <TouchableOpacity style={styles.acceptButton} onPress={() => this.takeOffer(item)}>
                        <Text style={{ color: "rgb(251,82,87)", fontSize: 18, fontWeight: 'bold' }}>Accept</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    takeOffer(item) {
        console.log('Take request');
        this.setState({ tempAccept: item, showPriceModal: true });
    }

    checkInput() {
        this.setState({ showPriceModal: false })
        if (this.state.price != '') {
            var str = this.state.price;
            // check if there is a not-a-number character
            for (var i = 0; i < str.length; i++) {
                // console.log(str.charAt(i).trim().length)

                if (str.charAt(i).trim().length == 0) {
                    alert('Please make sure there is no space between numbers');
                    return;
                }

                if (isNaN(str.charAt(i)) == true) {
                    console.log('NaN');
                    console.log(str.charAt(i))
                    if (str.charAt(i) != ".") {
                        alert('Please enter numbers and dot only (example : 12.45)');
                        return;
                    }
                }
            }
            this.sendOffer();
        } else {
            alert('Please enter price');
        }
    }

    sendOffer() {
        var self = this;
        self.setState({ showWaitingModal: true, pauseSearch: 'yes' })
        axios.post(this.state.ip + '/api/driver-sendOffer', {
            requestId: this.state.tempAccept.id,
            driverId: this.state.userDetail.id,
            price: this.state.price,
        }).then(function (respond) {
            console.log(respond);
            self.setState({ tempOffer: respond.data, waitingState: 'yes' });
        }).catch(function (error) {
            console.log(respond);
        });
    }

    cancelOffer() {
        var self = this;
        console.log(this.state.tempOffer);

        axios.post(this.state.ip + '/api/driver-cancelOffer', {
            offerId: this.state.tempOffer.id,
        }).then(function (respond) {
            console.log(respond)
            self.setState({ showWaitingModal: false, waitingState: 'no' })
        }).catch(function (error) {
            console.log(error)
        })
    }

    goToPassenger() {
        console.log('Going to passenger');
        console.log(this.state.tempAccept);
        this.setState({ showRequestContainer: false, showBottomContent: 'driver_otw', showHeadUpModal: false, calculateDistance: true });
    }

    openWaze() {
        console.log('Open Waze');
        var self = this;
        showLocation({
            app: 'waze',
            latitude: self.state.tempAccept.pickup_lat,
            longitude: self.state.tempAccept.pickup_lng,
        })
    }

    openWazeJobDestination() {
        console.log('Open Waze');
        var self = this;
        axios.post(this.state.ip + '/api/driver-getDestinationCoord', {
            ride_id: this.state.tempAccept.id,
        }).then(function (respond) {
            console.log(respond);
            showLocation({
                app: 'waze',
                latitude: respond.data[0].latitude,
                longitude: respond.data[0].longitude,
            })
        }).then(function (error) {
            console.log(error);
        })

        // showLocation({
        //     app: 'waze',
        //     latitude: self.state.tempOffer.pickup_lat,
        //     longitude: self.state.tempOffer.pickup_lng,
        // })
    }

    passengerOnBoard() {
        var self = this;
        axios.post(this.state.ip + '/api/driver-passengerOnBoard', {
            ride_id: this.state.tempAccept.id,
            // ride_id: 85,
        }).then(function (respond) {
            console.log(respond);
            self.setState({ tempDestCoord: respond.data[0] })
            self.setState({ showPickUpWarning: false, showBottomContent: 'destination_otw', whatDirection: 'destination' });
        }).catch(function (error) {
            console.log(error);
        });
    }

    endJob() {
        var self = this;
        this.setState({
            destinationName: '',
            currentPlaceName: '',
            driverCoord: '',
            showSpinner: false,
            showPriceModal: false,
            tempAccept: '',
            price: '',
            pauseSearch: 'no',
            tempOffer: '',
            showWaitingModal: false,
            waitingState: 'no',
            showHeadUpModal: false,
            showBottomContent: 'none',
            calculateDistance: false,
            distanceAchieve: false,
            showPickUpWarning: false,
            less100m: true,
            showRejectModal: false,
            showEndJobWarning: false,
            whatDirection: 'none',
            tempDestCoord: '',
            requestArr:[],
            showRequestContainer:true,
        });
        axios.post(this.state.ip+'/api/driver-jobComplete',{
            ride_id : this.state.tempAccept.id,
        }).then(function(respond){
            console.log(respond);
            // self.props.navigation.navigate('HomeDrawer');
        }).catch(function(error){
            console.log(error);
        })
    }

    render() {
        var self = this;

        const powerButtonRender = () => {

            if (this.state.power == 'off') {
                return (
                    <TouchableOpacity style={styles.powerButtonContainer} onPress={() => this.turnOnService()}>
                        <Image source={require('../assets/power.png')} style={{ width: 43, height: 45 }} />
                    </TouchableOpacity>
                )
            }
            if (this.state.power == 'on') {
                return (
                    <TouchableOpacity style={styles.powerButtonContainerOn} onPress={() => this.turnOffService()}>
                        <Image source={require('../assets/power_white.png')} style={{ width: 43, height: 45 }} />
                    </TouchableOpacity>
                )
            }
        }

        const showRequestContainer = () => {
            if (this.state.showRequestContainer == true) {
                return (
                    <View style={styles.enterLocationContainer}>
                        <FlatList
                            data={this.state.requestArr}
                            renderItem={this.renderRequestCard}
                            keyExtractor={item => item.id}
                            keyExtractor={item => item.id.toString()}
                            horizontal={true}
                        />
                    </View>
                )
            }
        }

        const showBottomContent = () => {
            if (this.state.showBottomContent == 'driver_otw') {
                // if (false) {
                return (
                    <View style={{ zIndex: 9, position: 'absolute', bottom: 0 }}>
                        <View style={{ position: 'relative', width: screenWidth, height: 75, }}>
                            <TouchableOpacity onPress={() => this.openWaze()} style={{
                                width: 70,
                                height: 70,
                                position: 'absolute',
                                right: 100,
                                borderRadius: 40,
                            }}>
                                <Image source={require('../assets/waze.png')} style={{
                                    width: 70,
                                    height: 70,
                                    // position: 'absolute',
                                    // right: 10,
                                    borderColor: 'rgb(251,82,87)',
                                    borderWidth: 2,
                                    borderRadius: 40,
                                }}></Image>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ showPickUpWarning: true })} style={{
                                width: 70,
                                height: 70,
                                backgroundColor: 'rgb(251,82,87)',
                                borderRadius: 40,
                                justifyContent: 'center',
                                alignItems: 'center',
                                position: 'absolute',
                                right: 10,
                            }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Pickup</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: screenWidth, backgroundColor: 'white', height: 200 }}>
                            <View style={{ position: 'relative', alignItems: 'center', height: 200, justifyContent: 'center' }}>
                                <View style={{ position: 'absolute', top: 10, }}>
                                    <Text style={{ color: "rgb(251,82,87)", fontSize: 18, fontWeight: 'bold' }}>Please go to pickup location.</Text>
                                </View>
                                <View>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                                </View>
                                <TouchableOpacity style={{
                                    width: screenWidth - 30,
                                    height: 40,
                                    backgroundColor: 'rgb(251,82,87)',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                    position: 'absolute',
                                    alignSelf: 'center',
                                    bottom: 10,
                                }}>
                                    <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )
            }
            if (this.state.showBottomContent == 'destination_otw') {
                // if (true) {
                let endjobButton = () => {
                    if (this.state.less100m == true) {
                        return (
                            <TouchableOpacity onPress={() => { this.setState({ showEndJobWarning: true }) }} style={{
                                width: screenWidth - 30,
                                height: 40,
                                backgroundColor: 'rgb(251,82,87)',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 20,
                                position: 'absolute',
                                alignSelf: 'center',
                                bottom: 10,
                            }}>
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Finish Job</Text>
                            </TouchableOpacity>
                        )
                    }
                };

                return (
                    <View style={{ zIndex: 9, position: 'absolute', bottom: 0 }}>
                        <View style={{ position: 'relative', width: screenWidth, height: 75, }}>
                            <TouchableOpacity onPress={() => this.openWazeJobDestination()} style={{
                                width: 70,
                                height: 70,
                                position: 'absolute',
                                right: 10,
                                borderRadius: 40,
                            }}>
                                <Image source={require('../assets/waze.png')} style={{
                                    width: 70,
                                    height: 70,
                                    // position: 'absolute',
                                    // right: 10,
                                    borderColor: 'rgb(251,82,87)',
                                    borderWidth: 2,
                                    borderRadius: 40,
                                }}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: screenWidth, backgroundColor: 'white', height: 200 }}>
                            <View style={{ position: 'relative', alignItems: 'center', height: 200, justifyContent: 'center' }}>
                                <View style={{ position: 'absolute', top: 10, }}>
                                    <Text style={{ color: "rgb(251,82,87)", fontSize: 18, fontWeight: 'bold' }}>Please go to the job's destination</Text>
                                </View>
                                <View>
                                    <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                                </View>
                                {endjobButton()}
                            </View>
                        </View>
                    </View>
                )
            }
        }

        const showDirection = () => {
            if (this.state.whatDirection == 'passenger') {
                return (
                    <MapViewDirections
                        origin={{ latitude: this.state.markerCoord.latitude, longitude: this.state.markerCoord.longitude }}
                        destination={{ latitude: this.state.tempAccept.pickup_lat, longitude: this.state.tempAccept.pickup_lng }}
                        apikey={'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI'}
                        strokeWidth={5}
                        strokeColor="rgba(251,82,87,0.8)"
                        onError={(errorMessage) => {
                            console.log('errorMessage');
                        }}
                    />
                )
            }
            if (this.state.whatDirection == 'destination') {
                return (
                    <MapViewDirections
                        origin={{ latitude: this.state.markerCoord.latitude, longitude: this.state.markerCoord.longitude }}
                        destination={{ latitude: this.state.tempDestCoord.latitude, longitude: this.state.tempDestCoord.longitude }}
                        apikey={'AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI'}
                        strokeWidth={5}
                        strokeColor="rgba(251,82,87,0.8)"
                        onError={(errorMessage) => {
                            console.log('errorMessage');
                        }}
                    />
                )
            }

        }

        return (
            <View style={styles.container}>
                {/* <TouchableOpacity style={styles.menuContainer} onPress={() => this.props.navigation.goBack()}>
                    <Image style={styles.menuButton} source={require('../assets/chevron_left_withgradient.png')} />
                </TouchableOpacity> */}

                {/* This is for ActivityIndicator aka Spinner */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showSpinner}
                >
                    <View style={styles.spinnerContainer}>
                        <View style={styles.spinnerModalContent}>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" />
                        </View>
                    </View>
                </Modal>

                {/* Show Waiting for Passenger respond modal */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showWaitingModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.waitingModalContainer}>
                            <Text style={{ marginTop: 12, fontWeight: 'bold' }}>Waiting for passenger respond</Text>
                            <Text style={{ marginBottom: 15 }}>Please wait...</Text>
                            <ActivityIndicator size="large" animating={true} color="rgb(251,82,87)" style={{ marginBottom: 15 }} />
                            <TouchableOpacity style={{
                                width: '100%',
                                backgroundColor: "rgb(251,82,87)",
                                height: 45,
                                borderBottomLeftRadius: 20,
                                borderBottomRightRadius: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }} onPress={() => this.cancelOffer()}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* This is modal for entering price */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showPriceModal}
                >
                    <View style={styles.priceModalContainer}>
                        <View style={styles.priceModalContent}>
                            <Text style={{ color: "rgb(251,82,87)", fontWeight: 'bold', fontSize: 18, marginBottom: 20 }}>Please enter offered price:</Text>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Item rounded style={{ width: '70%', height: 40, backgroundColor: 'rgba(0,0,0,0.1)', marginRight: 10 }}>
                                    <Input
                                        placeholder="Price (RM)"
                                        placeholderTextColor="rgb(251,82,87)"
                                        style={{ color: "rgb(251,82,87)", fontSize: 18, fontWeight: 'bold' }}
                                        keyboardType="numeric"
                                        onChangeText={(price) => this.setState({ price })} />
                                </Item>
                                <TouchableOpacity
                                    style={{ backgroundColor: "rgb(251,82,87)", width: '30%', height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() => this.checkInput()}
                                >
                                    <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Submit</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                {/* Show modal for Please pickup your passenger */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showHeadUpModal}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.waitingContent}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: "rgb(251,82,87)", marginBottom: 20, marginTop: 10 }}>Heads Up!</Text>
                            <Text>Passenger accepted your request. </Text>
                            <Text style={{ marginBottom: 30 }}>Please go to the pickup location.</Text>
                            <TouchableOpacity onPress={() => this.goToPassenger()} style={{
                                width: '100%',
                                backgroundColor: "rgb(251,82,87)",
                                height: 40,
                                borderBottomLeftRadius: 20,
                                borderBottomRightRadius: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Understood</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Show modal for Offer rejected */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showRejectModal}
                // visible={true}
                >
                    <View style={styles.modalContainer}>
                        <View style={{
                            width: screenWidth - 100,
                            height: 200,
                            backgroundColor: 'white',
                            borderRadius: 20,
                            padding: 15,
                            justifyContent: 'center',
                            position: 'relative'
                        }}>
                            <View style={{ position: 'absolute', top: 15, left: 15 }}>
                                <Text style={{ color: "rgb(251,82,87)", fontSize: 24, fontWeight: 'bold' }}>Sorry</Text>
                            </View>
                            <View style={{ marginBottom: 30 }}>
                                <Text style={{ fontSize: 18 }}>Your offer was not selected</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ showRejectModal: false, pauseSearch: 'no' })
                                }}
                                style={{
                                    width: '100%',
                                    backgroundColor: "rgb(251,82,87)",
                                    height: 40,
                                    borderRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    position: 'absolute',
                                    bottom: 15,
                                    left: 15,
                                }}>
                                <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Okay</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* Warning modal for pressing pickup button */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showPickUpWarning}
                >
                    <View style={styles.modalContainer}>
                        <View style={{ width: screenWidth - 100, height: 200, backgroundColor: 'white', position: 'relative', padding: 15, justifyContent: 'center', borderRadius: 20 }}>
                            <View style={{ position: 'absolute', top: 10, left: 15 }}>
                                <Text style={{ fontSize: 24, fontWeight: 'bold', color: "rgb(251,82,87)" }}>Warning</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 18, marginBottom: 20 }}>Does the passenger is on board?</Text>
                            </View>
                            <View style={{ position: 'absolute', bottom: 10, left: 10, width: '100%' }}>
                                <View style={{ flexDirection: 'row', width: '100%', height: 40, position: 'relative' }}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ showPickUpWarning: false })}
                                        style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(251,82,87)', marginRight: 10, borderRadius: 20 }}
                                    >
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>No</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.passengerOnBoard()}
                                        style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(251,82,87)', borderRadius: 20 }}
                                    >
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Yes</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>

                {/* Warning modal for pressing End Job button */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    // visible={true}
                    visible={this.state.showEndJobWarning}
                >
                    <View style={styles.modalContainer}>
                        <View style={{ width: screenWidth - 100, height: 200, backgroundColor: 'white', position: 'relative', padding: 15, justifyContent: 'center', borderRadius: 20 }}>
                            <View style={{ position: 'absolute', top: 10, left: 15 }}>
                                <Text style={{ fontSize: 24, fontWeight: 'bold', color: "rgb(251,82,87)" }}>Warning</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 18, marginBottom: 20 }}>Job complete?</Text>
                            </View>
                            <View style={{ position: 'absolute', bottom: 10, left: 10, width: '100%' }}>
                                <View style={{ flexDirection: 'row', width: '100%', height: 40, position: 'relative' }}>
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ showEndJobWarning: false }) }}
                                        style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(251,82,87)', marginRight: 10, borderRadius: 20 }}
                                    >
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>No</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={()=>{this.endJob()}}
                                        style={{ width: '50%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(251,82,87)', borderRadius: 20 }}
                                    >
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Yes</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>

                <TouchableOpacity style={[styles.gpsContainer, this.state.checkArr ? { bottom: 250 } : { bottom: 50 }]} onPress={() => this.reRender()}>
                    <Image style={styles.gpsButton} source={require('../assets/gps.png')} />
                </TouchableOpacity>

                <View style={{ width: screenWidth, position: 'absolute', top: 0, zIndex: 9 }}>
                    <View style={styles.header}>
                        <TouchableOpacity style={{ width: 40, height: 40, position: 'absolute', left: 10 }} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../assets/chevron_left.png')} style={{ width: 40, height: 40 }} />
                        </TouchableOpacity>

                        {powerButtonRender()}

                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18, position: 'absolute', right: 10 }}>Driver service {this.state.serviceText}</Text>
                    </View>
                </View>

                <View style={[styles.mapContainer, this.state.checkArr ? { height: screenHeight - 300 } : { height: screenHeight, backgroundColor: 'pink' }]}></View>
                {/* <MapView
                    // style={[styles.mapContainer, this.state.checkArr ? { height: screenHeight - 300 } : { height: screenHeight }]}
                    style={[styles.mapContainer, { height: screenHeight }]}
                    initialRegion={{
                        latitude: this.state.markerCoord.latitude,
                        longitude: this.state.markerCoord.longitude,
                        latitudeDelta: 0.007,
                        longitudeDelta: 0.007,
                    }}
                    ref={map => { this.map = map }}
                    // showsUserLocation={true}
                    onRegionChangeComplete={(region) => this.destinationMarker(region)}
                >
                    {showDirection()}
                </MapView> */}

                {showRequestContainer()}
                {showBottomContent()}

                <View style={styles.markerFixed}>
                    <Image style={styles.marker} source={require('../assets/destination_marker.png')} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        width: screenWidth,
        height: 70,
        backgroundColor: "rgb(251,82,87)",
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        position: 'relative',
        // flex:1,
    },
    powerButtonContainer: {
        width: 100,
        height: 60,
        backgroundColor: 'white',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        // position:'absolute',
        // left: (screenWidth/2)-25,
    },
    powerButtonContainerOn: {
        width: 100,
        height: 60,
        backgroundColor: "rgb(251,82,87)",
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mapContainer: {
        width: screenWidth,
    },
    menuContainer: {
        zIndex: 9,
        position: 'absolute',
        top: 40,
        left: 20,
    },
    menuButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'white',
        borderWidth: 2,
    },
    gpsContainer: {
        zIndex: 10,
        position: 'absolute',
        left: 20,
    },
    gpsButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'white',
        borderWidth: 2,
    },
    enterLocationContainer: {
        width: screenWidth,
        height: 230,
        backgroundColor: 'rgba(255,255,255,0)',
        padding: 10,
        position: 'absolute',
        bottom: 0,
        zIndex: 9,
    },
    markerFixed: {
        left: '50%',
        marginLeft: -24,
        marginTop: -48,
        position: 'absolute',
        top: '50%'
    },
    marker: {
        height: 48,
        width: 48
    },
    viewMoreButton: {
        position: 'absolute',
        right: 10,
        backgroundColor: "rgb(251,82,87)",
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        borderRadius: 20,
    },
    requestCardContainer: {
        width: screenWidth - 80,
        height: 210,
        backgroundColor: 'rgb(251, 181, 182)',
        marginRight: 20,
        padding: 10,
        // position: 'absolute',
        // bottom: 0,
        // zIndex: 9,
    },
    acceptButton: {
        position: 'absolute',
        right: 10,
        height: 40,
        width: 100,
        bottom: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
    },
    spinnerContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    spinnerModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 100,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    priceModalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    priceModalContent: {
        alignItems: "center",
        justifyContent: "center",
        width: 400,
        height: 150,
        backgroundColor: 'white',
        borderRadius: 20,
        flexDirection: 'column',
        padding: 20,
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    waitingModalContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: 200,
        height: 150,
        backgroundColor: 'white',
        borderRadius: 20,
        position: 'relative',
    },
    waitingContent: {
        alignItems: "center",
        // justifyContent: "center",
        width: 200,
        height: 150,
        backgroundColor: 'white',
        borderRadius: 20,
        position: 'relative',
    }
})