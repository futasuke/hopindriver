import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
} from 'react-native';
import MapView from 'react-native-maps';
import { createAppContainer, StackActions, NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './view/Login';
import RegisterScreen from './view/Register';
import HomeScreen from './view/Home';
import SettingScreen from './view/Setting';
import MenuDrawer from './component/Sidebar';
import DriverRegisterScreen from './view/DriverRegister';
import FirstDetailScreen from './view/FirstDetail';
import EditProfile from './view/EditProfile';
import ContactUs from './view/ContactUs';
import Feedback from './view/Feedback';
import News from './view/News';
import Fact from './view/Fact';
import Adsinfo from './view/Adsinfo';
import MainMap from './view/MainMap';

const loginStack = createStackNavigator();
const mainDrawer = createDrawerNavigator();
const MapStack = createStackNavigator();
const screenWidth = Dimensions.get("window").width;

export default class App extends React.Component {

  createMapStack(props) {
    return (
      <MapStack.Navigator initialRouteName="MainMap" headerMode="none">
        <MapStack.Screen name="MainMap" component={MainMap} initialParams={{
          ip: props.route.params.ip,
          coord: props.route.params.coord,
          currentCoord: props.route.params.coord,
          userDetail: props.route.params.userDetail,
        }} />
      </MapStack.Navigator>
    );
  }

  createMainDrawer(props) {
    var thisRoute = props.route;
    var self = this;
  
    return (
      <mainDrawer.Navigator initialRouteName="Home" drawerStyle={{ width: screenWidth * 0.7 }} drawerContent={props => { return (<MenuDrawer navigation={props.navigation} route={thisRoute} />) }}>
        <mainDrawer.Screen name="Home" component={HomeScreen} initialParams={{
          userId: props.route.params.userId,
          ip: props.route.params.ip,
          userDetail: props.route.params.userDetail,
        }} />
        <mainDrawer.Screen name="Setting" component={SettingScreen} />
      </mainDrawer.Navigator>
    );
  }

  // createMapStack(props){
  //   return(
  //     <MapStack.Navigator initialRouteName="MapOne">
  //       <MapStack.Screen name="MapOne" component={MainMap}/>
  //     </MapStack.Navigator>
  //   );
  // }

  render() {
    var self = this;

    return (
      <NavigationContainer>
        <loginStack.Navigator initialRouteName="Login" headerMode="none">
          <loginStack.Screen name="Login" component={LoginScreen} />
          <loginStack.Screen name="Register" component={RegisterScreen} />
          <loginStack.Screen name="FirstDetail" component={FirstDetailScreen} />
          <loginStack.Screen name="DriverRegister" component={DriverRegisterScreen} />
          <loginStack.Screen name="HomeDrawer" children={this.createMainDrawer} />
          <loginStack.Screen name="EditProfile" component={EditProfile} />
          <loginStack.Screen name="ContactUs" component={ContactUs} />
          <loginStack.Screen name="Feedback" component={Feedback} />
          <loginStack.Screen name="News" component={News} />
          <loginStack.Screen name="Adsinfo" component={Adsinfo} />
          <loginStack.Screen name="Fact" component={Fact} />
          <loginStack.Screen name="GoMap" children={this.createMapStack} />
        </loginStack.Navigator>
      </NavigationContainer>
    );
  }
}
