import React, { Component } from 'react';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    PermissionsAndroid,
    FlatList,
} from 'react-native';

import 'react-native-gesture-handler';
import Geolocation from 'react-native-geolocation-service';
import { SliderBox } from "react-native-image-slider-box";
import FastImage from 'react-native-fast-image';
import axios from 'axios';
import { NavigationEvents } from '@react-navigation/native';
import FactCourasel from '../component/FactCourasel';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class HomeBody extends React.Component {
    state = {
        ip: this.props.route.params.ip,
        newsCarousel: [],
    }

    getNews() {
        var self = this;
        axios.get(this.state.ip + '/api/try-getNews')
            .then(function (response) {
                self.setState({ newsCarousel: response.data[0].content });
                // self.setState({ userDetail: response.data[0].content });
                // self.setState({ dpImage: self.state.ip + self.state.userDetail.dpPath });
            }).catch(function (error) {
                console.log(error);
            });
    }

    goToNews(item) {
        var self = this;
        this.props.navigation.navigate('News', { news: item, ip: this.state.ip })
    }

    componentDidMount(){
        var self = this;

        console.log(this.props);

        this.onFocus = this.props.navigation.addListener('focus', () => {
            self.getNews();
        });
        this.onBlur = this.props.navigation.addListener('blur', () => {
            // self.setState({ dpImage: "" });
        });
    }

    componentWillUnmount() {
        this.onFocus();
        this.onBlur();
    }

    _renderItem = ({ item, index }) => {
        var self = this;
        return (
            <TouchableOpacity onPress={() => this.goToNews(item)}>
                <View style={styles.newsContainer}>
                    <FastImage style={styles.newsImage} source={{ uri: self.state.ip + "/" + item.picPath, cache: FastImage.cacheControl.web, priority: FastImage.priority.normal, }} />
                    {/* <Text>{item.title}</Text> */}
                    <View style={styles.newsContent}>
                        <Text style={styles.newsTitle}>{item.title}</Text>
                        <View style={styles.newsDescription}>
                            <Text numberOfLines={1}>{item.description}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <ScrollView scrollEnabled={true} contentContainerStyle={{paddingTop:20}}>
                <FactCourasel route={this.props.route} navigation={this.props.navigation} />
                <View style={styles.newsCard}>
                    <Text style={styles.cardTitle}>News</Text>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        data={this.state.newsCarousel}
                        horizontal={true}
                        renderItem={this._renderItem}
                        style={{ width: screenWidth - 85 }}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
                <View style={styles.advertisementCard}>
                    <Text style={styles.cardTitle}>Advertisement</Text>
                </View>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    newsCard: {
        backgroundColor: 'white',
        width: screenWidth - 50,
        height: 300,
        borderRadius: 20,
        marginTop: 20,
        // flexDirection: 'row',
        paddingTop: 10,
        paddingLeft: 20,
        // alignItems: 'center',
        alignSelf: "center",
        // marginBottom:20,
    },
    advertisementCard: {
        backgroundColor: 'white',
        width: screenWidth - 50,
        height: 300,
        borderRadius: 20,
        marginTop: 20,
        // flexDirection: 'row',
        paddingTop: 10,
        paddingLeft: 20,
        // alignItems: 'center',
        alignSelf: "center",
        marginBottom:60,
    },
    cardTitle: {
        fontWeight: 'bold',
        fontSize: 22,
        color: "rgb(251,82,87)",
    },
    newsContainer: {
        // width:screenWidth - 85,
        // height:300,
        // backgroundColor:'black',
        // flex: 1,
        borderColor: 'grey',
        borderWidth: 2,
        borderRadius: 20,
        marginRight: 10,
        marginTop: 15,
        height: 220,
    },
    newsImage: {
        height: 150,
        width: 250,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    newsTitle: {
        fontWeight: 'bold',
    },
    newsContent: {
        marginTop: 10,
        marginLeft: 10,
    },
    newsDescription: {
        marginTop: 10,
    },
    topBgStlye: {
        backgroundColor: "rgb(251,82,87)",
        width: screenWidth,
        height: '18%',
        zIndex: -1,
        position: 'absolute',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    factAdContainer:{
        borderColor: 'grey',
        borderWidth: 2,
        marginRight: 10,
        marginTop: 15,
        height: 150,
    },
    factAdImage:{
        height: 150,
        width: 400,
    }
})